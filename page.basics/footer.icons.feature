# Front Page - Footer - Social Icons

@footer-icons
   Feature: Footer  - Social icons

   Background:
     Given I am at "/"

   Scenario: Facebook Icon
     When I click on the element "a.westernIcons-FacebookIcon"
     Then the response status code should be 200

   Scenario: Flickr 
     When I click on the element "a.westernIcons-FlickrIcon"
     Then the response status code should be 200

   Scenario: Twitter 
     When I click on the element "a.westernIcons-TwitterIcon"
     Then the response status code should be 200

   Scenario: youTube 
     When I click on the element "a.westernIcons-YouTubeIcon"
     Then the response status code should be 200

   Scenario: Google+ 
     When I click on the element "a.westernIcons-GooglePlusIcon"
     Then the response status code should be 200

   Scenario: RSS
     When I click on the element "a.westernIcons-RSSicon"
     Then the response status code should be 200
