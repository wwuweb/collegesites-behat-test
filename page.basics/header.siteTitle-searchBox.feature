# header title and search icon/box

@header-title
Feature: check the presence of the header title text
  In order to see if the home page loaded correctly
  As a guest
  I need to check the title box and if the string 'Test'

  Background:
    Given I am at "/"

  Scenario: Check to see of there is text in the header (school name/site title)
    When I visit "/"
    Then the ".site-name" element should contain "Test"


  @javascript
  Scenario: click the search icon (class = western-search), input box should expand
    When I press the "Open Search" button
    Then I should see "Search Western"
