# footer features
Feature: test for the presence of the contact box, privacy link in the footer
   @contact-box
   Scenario: check for presence of the contact box on the index page
      Given I am on "/"
      Then I should see "516 High Street"
      And I should see "(360) 650-3000" 
      And I should see the link "Contact Western"
   Scenario: test the contact info box e-mail link
      Given I am at "/"
      When I follow "Contact Western"
      Then I should see the heading "Contact Us"
    @privacy-link
    Scenario: Check for privacy link
      Given I am at "/"
      When I click "Website Privacy Statement"
      Then I should see the heading "Online Privacy Statement"

