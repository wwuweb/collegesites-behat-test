# features/wwumenu.feature

Feature:  Test of the top right header menu, id="wwumenu"
  In order to test the far upper right menu
  As a guest
  I need to click through all the seelctions and go to the proper page

  Scenario: test the calendar link
    Given I am at "/"
    When I click "Calendar"
    Then I should see the heading "Academic Calendar"

  Scenario: test the directory link
    Given I am at "/"
    When I click "Directory"
    Then I should see the heading "Campus Directory"

  Scenario: test the index link
    Given I am at "/"
    When I click "Index"
    Then I should see the heading "Alphabetic Index"

  Scenario: test the map link
    Given I am at "/"
    When I click "Map"
    Then I should see the heading "Campus Maps"

  Scenario: test the myWestern link
    Given I am at "/"
    When I click "myWestern"
    Then I should see the text "myWestern: Home"

