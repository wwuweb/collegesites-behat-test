# Routines to preset parts of the site through the "front panel"

@preset
   Feature: Drush pseudo-users need to log in and log off, detected by 'log in ' and 'log out' text on the screen specified in the behat.yml file.
   Our style, sadly, does not have this text normally on the screen, but it is visible in the toolbar.  The toolbar is normally disabled for non-admins.
   This script enables the toolbar for non-admin users so the drush command will work. Repeat as needed for various user roles  Bug #2023635

   @api
   Scenario: Enable the admin toolbar for Authenticated Users (id=2)
         so 'log out' can be seen (to fix login bug 2023625)
     Given I am logged in as a user with the "administrator" role
       And I visit "/admin/people/permissions/2"
       And I check the box "edit-2-access-administration-menu"
     When I press "Save permissions"
     Then I should see "The changes have been saved."

