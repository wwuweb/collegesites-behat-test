Feature: greek extension
  Checks greek letters in wysiwyg

  @api @javascript
  Scenario: Untagged scenario uses blackbox driver and fails
    Given I am logged in as a user with the "administrator" role
    And I visit "/node/add/page"
    Then I should see "Body"
    Then I should see "Insert Special Character"
    When I click on the element ".cke_button_icon.cke_button__specialchar_icon"
    Then I should see "Select Special Character"
    Then I should see "ß"
